package no.uib.inf101.terminal;

public interface Command {
    
    //String[] args = new String[] {"foo","bar"};
    //String result = command.run(args);
    

    String run(String[] args);

    String getName();
}
